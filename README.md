# JHTTP

## What is it ?

JHTTP is a small library to output JSON in HTTP handlers.

## How to import it ?

```bash
go get gitlab.com/melvin.biamont/jhttp
```

## How to use it ?

```go
// You can give an object as a response
jhttp.Success(w, anObject)

// Or you can specify the status code
jhttp.Response(w, anObject, http.StatusInternalServerError)

// You can output directly {"error" : "Your message here"} 
jhttp.Error(w, "Your message here", http.StatusInternalServerError)

// You can output directly {"status" : "Your message here"} 
jhttp.Status(w, "Your message here", http.StatusInternalServerError)
```