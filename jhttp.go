package jhttp

import (
	"net/http"
	"encoding/json"
)

type errorWithMessage struct {
	Error string `json:"error"`
}

type errorWithCode struct {
	Error string `json:"error"`
	Code  int64 `json:"code"`
}

type statusMessage struct {
	Status string `json:"status"`
}

func Response(w http.ResponseWriter, obj interface{}, code int) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)

	val, err := json.Marshal(obj)

	if err != nil {
		return err
	}

	w.Write(val)

	return nil
}

func Error(w http.ResponseWriter, error string, code int) {
	Response(w, errorWithMessage{Error: error}, code)
}

func ErrorCode(w http.ResponseWriter, error string, errorCode int64, httpCode int) {
	Response(w, errorWithCode{Error: error, Code: errorCode}, httpCode)
}

func Status(w http.ResponseWriter, status string, code int) {
	Response(w, statusMessage{Status: status}, code)
}

func Success(w http.ResponseWriter, obj interface{}) {
	Response(w, obj, 200)
}
